import os
import shutil

src_path = "F:/vinAI/water-level/data/annotations/vid1"
for filename in os.listdir(src_path):
    if "txt" in filename:
        filepath = src_path+"/"+filename
        data_read = open(filepath, "r")
        line = data_read.readlines()
        line[0] = line[0].replace("15", "0", 1)
        data_write = open(filepath, "w")
        data_write.writelines(line)
        data_read.close()
        data_write.close()
